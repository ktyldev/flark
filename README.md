# Flark

Ray tracing rendering experiments

https://raytracing.github.io/

Produced PPM images can be converted to PNG with `imagemagick`:

```
convert image.ppm image.png
```
